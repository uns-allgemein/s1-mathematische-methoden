load Temperatures

% Execute in Octave (unnecessary with MATLAB)
pkg load statistics

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 1: Set up the functional model %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n_obs = length(l);              % Number of observations
t = [1:n_obs]';

% The model functions are given by
% f_i(x) = a*t_i + b
% where the parameter vector is x = [a;b]
m = 2;                          % Number of parameters
r = n_obs - m;                  % Redundancy

A = [t ones(n_obs,1)];          % Design matrix

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 2: Set up the stochastic model %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
P = eye(n_obs);
% As we have P = I, we omit P in the following steps

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 3: Set up the normal equations %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
N = A'*A;                       % Normal equation matrix
n = A'*l;                       % Right-hand side

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 4: Inversion of the normal equations %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Q_xs = inv(N);          % Cofactor matrix of the estimated parameters

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 5: Solution of the normal equations %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xs = Q_xs*n;            % Parameter estimates
as = xs(1); bs = xs(2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 6: Computation of the residuals %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
vs = A*xs - l;          % Estimated residuals
A'*vs                   % Null validation

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 7: Computation of the covariance matrix of the residuals %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Q_vs = inv(P) - A*Q_xs*A';  % Cofactor matrix of the estimated residuals
ri = diag(Q_vs);            % Partial redundancies

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 8: Computation of the residual square sum %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Omega = vs'*vs;             % Residual square sum
sigmasq = Omega/r;          % Estimate of the variance factor

C_xs = sigmasq*Q_xs;        % Covariance matrix of the estimated parameters
C_vs = sigmasq*Q_vs;        % Covariance matrix of the estimated residuals

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 9: Main validation %
%%%%%%%%%%%%%%%%%%%%%%%%%%%
ls = l + vs;                % Adjusted Observations
ls(1) - as*t(1) - bs        % Should be close to 0!
ls(2) - as*t(2) - bs
ls(3) - as*t(3) - bs
ls(4) - as*t(4) - bs

%%%%%%%%%%%%%%%%%%
% Parameter Test %
%%%%%%%%%%%%%%%%%%
T = xs(1)/sqrt(C_xs(1,1));  % Value of the test statistic
k1 = tinv(0.025,r); 
k2 = tinv(0.975,r);
[k1 T k2]
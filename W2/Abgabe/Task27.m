fgA = [-1 1 ; 1 1]
fgb = [1 ; 1]
l1 = linsolve(fgA,fgb)
l1Inv = inv(fgA)*fgb
% Das Ergebnis von l1 ist [0 ; 1] und stimmt somit mit dem Ergebnis von l1Inv überein.
% Dieses Ergebnis ist die Einermenge

fhA = [-1 1 ; -1 1]
fhb = [1 ; 2]
l2 = linsolve(fhA,fhb)
l2Inv = inv(fhA)*fhb
% Das Ergebnis von l2 ist [-0.75 ; 0.75] dieses stimmt nicht mit dem Ergebnis von l2Invo überein welches [Inf ; Inf] ist.
% Dieses Ergebnis kann durch die blose betrachtung von linsolve und oder invertierten Koeffizientenmatrix bestimmt werden.

ff1A = [ -1 1 ; -1 1]
ff1b = [1 ; 1]
l3 = linsolve(ff1A,ff1b)

l3Inv = inv(ff1A)*ff1b
% Das Ergebnis von l3 ist [-0.5 ; 0.5] dieses stimmt nicht mit dem Ergebnis von l3Invo überein welches [Inf ; Inf] ist.
% Dieses Ergebnis kann durch die blose betrachtung von linsolve und oder invertierten Koeffizientenmatrix bestimmt werden.

% Inf wird von Octave als unendlich viele Werte betrachtet.